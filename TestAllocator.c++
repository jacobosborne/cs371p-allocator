// ----------------------------------------
// projects/c++/allocator/TestAllocator.c++
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test
{
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types <
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100 >>
    my_types_1;

TYPED_TEST_CASE (TestAllocator1, my_types_1);

TYPED_TEST (TestAllocator1, test_1)
{
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate (s);

    if (p != nullptr)
    {
        x.construct (p, v);
        ASSERT_EQ (v, *p);
        x.destroy (p);
        x.deallocate (p, s);
    }
}

TYPED_TEST (TestAllocator1, test_10)
{
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate (s);

    if (b != nullptr)
    {
        pointer e = b + s;
        pointer p = b;

        try
        {
            while (p != e)
            {
                x.construct (p, v);
                ++p;
            }
        }
        catch (...)
        {
            while (b != p)
            {
                --p;
                x.destroy (p);
            }

            x.deallocate (b, s);
            throw;
        }

        ASSERT_EQ (s, std::count (b, e, v) );

        while (b != e)
        {
            --e;
            x.destroy (e);
        }

        x.deallocate (b, s);
    }
}

// --------------
// TestAllocator2
// --------------

TEST (TestAllocator2, const_index)
{
    const my_allocator<int, 100> x;
    ASSERT_EQ (x[0], 92);
}

TEST (TestAllocator2, index)
{
    my_allocator<int, 100> x;
    ASSERT_EQ (x[0], 92);
}

// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test
{
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types <
my_allocator<int,    100>,
             my_allocator<double, 100 >>
             my_types_2;

TYPED_TEST_CASE (TestAllocator3, my_types_2);

TYPED_TEST (TestAllocator3, test_1)
{
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate (s);

    if (p != nullptr)
    {
        x.construct (p, v);
        ASSERT_EQ (v, *p);
        x.destroy (p);
        x.deallocate (p, s);
    }
}

TYPED_TEST (TestAllocator3, test_10)
{
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate (s);

    if (b != nullptr)
    {
        pointer e = b + s;
        pointer p = b;

        try
        {
            while (p != e)
            {
                x.construct (p, v);
                ++p;
            }
        }
        catch (...)
        {
            while (b != p)
            {
                --p;
                x.destroy (p);
            }

            x.deallocate (b, s);
            throw;
        }

        ASSERT_EQ (s, std::count (b, e, v) );

        while (b != e)
        {
            --e;
            x.destroy (e);
        }

        x.deallocate (b, s);
    }
}


// --------------
// --------------
// MY TESTS
// --------------
// --------------

// --------
// allocate-deallocate
// --------

TEST (allocate_deallocate, longish)
{
    my_allocator<int, 1000> alloc;
    int* p1 = alloc.allocate (5);
    int* p2 = alloc.allocate (5);
    alloc.deallocate (p1, 5);
    int* p3 = alloc.allocate (6);

    ASSERT_EQ (p1, &alloc[0] + 1);
    ASSERT_EQ (p2, &alloc[0] + 1 + 5 + 1 + 1);
    ASSERT_EQ (p3, &alloc[0] + 1 + 5 + 2 + 5 + 1 + 1);
    ASSERT_NE (p3, p1);
}

TEST (allocate_deallocate, coalesce_below)
{
    my_allocator<int, 1000> alloc;
    int* p1 = alloc.allocate (5);
    int* p2 = alloc.allocate (5);
    int* p3 = alloc.allocate (5);

    alloc.deallocate (p1, 5);
    alloc.deallocate (p2, 5);

    ASSERT_EQ (* (p1 - 1), 48);
}

TEST (allocate_deallocate, coalesce_above)
{
    my_allocator<int, 1000> alloc;
    int* p1 = alloc.allocate (5);
    int* p2 = alloc.allocate (5);
    int* p3 = alloc.allocate (5);

    alloc.deallocate (p2, 5);
    alloc.deallocate (p1, 5);

    ASSERT_EQ (* (p1 - 1), 48);
}

TEST (allocate_deallocate, coalesce_both_directions)
{
    my_allocator<int, 1000> alloc;
    int* p1 = alloc.allocate (5);
    int* p2 = alloc.allocate (5);
    int* p3 = alloc.allocate (5);
    int* p4 = alloc.allocate (5);

    alloc.deallocate (p1, 5);
    alloc.deallocate (p3, 5);
    alloc.deallocate (p2, 5);

    ASSERT_EQ (* (p1 - 1), 76);
}

// ---------
// bad_alloc
// ---------

TEST (bad_alloc, zero)
{
    my_allocator<long, 100> alloc;

    try
    {
        alloc.allocate (0);
        ASSERT_TRUE (false);
    }
    catch (std::exception& e)
    {
        ASSERT_EQ (strcmp ("std::bad_alloc", e.what() ), 0);
    }
}

TEST (bad_alloc, n_to_high)
{
    my_allocator<long, 100> alloc;

    try
    {
        alloc.allocate (100);
        ASSERT_TRUE (false);
    }
    catch (std::exception& e)
    {
        ASSERT_EQ (strcmp ("std::bad_alloc", e.what() ), 0);
    }
}

TEST (bad_alloc, insufficient_space)
{
    my_allocator<int, 100> alloc;
    int* p = alloc.allocate (20);

    try
    {
        alloc.allocate (10);
        ASSERT_TRUE (false);
    }
    catch (std::exception& e)
    {
        ASSERT_EQ (strcmp ("std::bad_alloc", e.what() ), 0);
    }

    ASSERT_EQ (p, &alloc[0] + 1);
}

// -----------
// bad_dealloc
// -----------

TEST (bad_dealloc, below_memory)
{
    my_allocator<int, 100> alloc;

    try
    {
        alloc.deallocate (&alloc[0] - 100, 1);
        ASSERT_TRUE (false);
    }
    catch (std::exception& e)
    {
        ASSERT_STREQ (e.what(), "Pointer to deallocate is outside (below) memory!");
    }
}

TEST (bad_dealloc, above_memory)
{
    my_allocator<int, 100> alloc;

    try
    {
        alloc.deallocate (&alloc[99] + 100, 1);
        ASSERT_TRUE (false);
    }
    catch (std::exception& e)
    {
        ASSERT_STREQ (e.what(), "Pointer to deallocate is outside (above) memory!");
    }
}

TEST (bad_dealloc, wrong_size)
{
    my_allocator<int, 100> alloc;
    int* p = alloc.allocate (5);

    try
    {
        alloc.deallocate (p, 3);
        ASSERT_TRUE (false);
    }
    catch (std::exception& e)
    {
        ASSERT_STREQ (e.what(), "Pointer is not start of block!");
    }
}

TEST (bad_dealloc, corrupted_top_sig)
{
    my_allocator<int, 100> alloc;
    int* p = alloc.allocate (5);
    * (p + 5) = -3;

    try
    {
        alloc.deallocate (p, 5);
        ASSERT_TRUE (false);
    }
    catch (std::exception& e)
    {
        ASSERT_STREQ (e.what(), "End of block is not actually the end of a block!");
    }
}
