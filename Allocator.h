// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

#include "stdafx.h"

#ifdef SHOW_DEBUG
#include <iostream>
using std::cout;
using std::endl;
#endif

#include <vector>
#include <list>

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator
{

#define BEGINNING_POINTER ( const_cast<int*>( &( (*this)[0] ) ) )
#define ENDING_POINTER    ( const_cast<int*>( &( (*this)[0] ) + ( N / sizeof(int) ) ) )

    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&)
    {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs)
    {
        return ! (lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const
    {
        #ifdef SHOW_DEBUG
        cout << "Checking validity..." << endl;
        #endif

        int absolute_sum = 0;
        iterator beginning = BEGINNING_POINTER;
        iterator ending    = ENDING_POINTER;

        #ifdef SHOW_DEBUG
        cout << "    BEGINNING_POINTER: " << BEGINNING_POINTER << endl;
        cout << "    ENDING_POINTER:    " << ENDING_POINTER    << endl;

        #ifdef SHOW_VALID

        for (int* current = BEGINNING_POINTER; current != ENDING_POINTER; current++)
        {
            cout << std::hex << current << ": " << *current << endl;
        }

        #endif
        #endif

        for (iterator current = beginning; current != ending; current++)
        {
            int block_length = abs (*current);
            int* bottom_signature = current.get_pointer_to_bottom_signature();
            int* top_signature = current.get_pointer_to_top_signature();

            if (bottom_signature >= ENDING_POINTER)
            {
                #ifdef SHOW_DEBUG
                cout << "Bottom signature is outside (above) memory!" << endl;
                #endif
                return false;
            }
            else if (bottom_signature < BEGINNING_POINTER)
            {
                #ifdef SHOW_DEBUG
                cout << "Bottom signature is outside (below) memory!" << endl;
                #endif
                return false;
            }
            else if (top_signature >= ENDING_POINTER)
            {
                #ifdef SHOW_DEBUG
                cout << "Top signature is outside (above) memory!" << endl;
                #endif
                return false;
            }
            else if (top_signature < BEGINNING_POINTER)
            {
                #ifdef SHOW_DEBUG
                cout << "Top signature is outside (below) memory!" << endl;
                #endif
                return false;
            }
            else if (*bottom_signature != *top_signature)
            {
                #ifdef SHOW_DEBUG
                cout << "Top and Bottom Signatures have different lengths!" << endl <<
                     "Bottom: " << *bottom_signature << "; Top: " << *top_signature << endl <<
                     "Bottom Location: " << bottom_signature << "; Top Location: " << top_signature << endl;
                #endif
            }
            else if (abs (*bottom_signature) != block_length)
            {
                #ifdef SHOW_DEBUG
                cout << "Bottom Signature has wrong length!" << endl;
                #endif
                return false;
            }
            else if (abs (*top_signature) != block_length)
            {
                #ifdef SHOW_DEBUG
                cout << "Top Signature has wrong length!" << endl;
                #endif
                return false;
            }
        }

        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator
    {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs)
        {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs)
        {
            return ! (lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p)
        {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int operator * () const
        {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ ()
        {
            int block_length = abs (*_p) / sizeof (int);
            _p += block_length + 2;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int)
        {
            iterator x = *this;
            ++*this;
            return x;
        }

        int* get_pointer_to_bottom_signature()
        {
            return _p;
        }

        int* get_pointer_to_top_signature()
        {
            return (_p + 1) + ( abs (*_p) / sizeof (int) );
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator ()
    {
        #ifdef SHOW_DEBUG

        for (int i = 0; i < N; i++)
        {
            a[i] = 0;
        }

        #endif

        // Set up the memory as initially one giant empty block
        *BEGINNING_POINTER    = N - 8;
        * (ENDING_POINTER - 1) = N - 8;

        assert (valid() );
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n)
    {
        int block_size = n * sizeof (T);

        #ifdef SHOW_DEBUG
        cout << std::dec << "Size of block to allocate: " << n * sizeof (T) << " bytes." << endl;
        #endif

        if (block_size < sizeof (T) )
        {
            throw std::bad_alloc();
        }
        else if (n + 2 * sizeof (int) > N)
        {
            throw std::bad_alloc();
        }

        iterator ending  = ENDING_POINTER;
        iterator current = BEGINNING_POINTER;

        pointer return_pointer = nullptr;

        while (current != ending)
        {
            #ifdef SHOW_DEBUG
            cout << "Checking block with sentinel " << *current << endl;
            #endif

            if (*current >= block_size)
            {

                int  old_block_length             = *current;
                int* old_block_bottom_signature   = current.get_pointer_to_bottom_signature();
                int* old_block_top_signature      = current.get_pointer_to_top_signature();

                int  fill_block_length_in_ints    = round_up_division (block_size, sizeof (int) );
                int  fill_block_length            = fill_block_length_in_ints * sizeof (int);
                int* fill_block_bottom_signature  = old_block_bottom_signature;
                int* fill_block_top_signature     = fill_block_bottom_signature + fill_block_length_in_ints + 1;

                #ifdef SHOW_DEBUG
                cout << "fill_block_length_in_ints:" << fill_block_length_in_ints << " fill_block_length:" << fill_block_length << endl            ;
                #endif

                int  empty_block_length           = old_block_length - ( (fill_block_length_in_ints + 2) * sizeof (int) );
                int* empty_block_bottom_signature = fill_block_top_signature + 1;
                int* empty_block_top_signature    = old_block_top_signature;

                *fill_block_bottom_signature  = -fill_block_length;
                *fill_block_top_signature     = -fill_block_length;

                if ( empty_block_length >= 0)
                {
                    *empty_block_bottom_signature = empty_block_length;
                    *empty_block_top_signature    = empty_block_length;
                }

                return_pointer = reinterpret_cast<pointer> (fill_block_bottom_signature + 1);
                break;
            }

            ++current;
        }

        if (current == ending)
        {
            throw std::bad_alloc();
        }

        assert (valid() );
        return return_pointer;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v)
    {
        new (p) T (v);                              // this is correct and exempt
        assert (valid() );
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type n)
    {
        int block_size = n * sizeof (T);

        #ifdef SHOW_DEBUG
        cout << std::dec << "Size of block to deallocate: " << block_size << " bytes." << endl;
        #endif

        int* bottom_signature = reinterpret_cast<int*> (p) - 1;
        int* top_signature    = reinterpret_cast<int*> (p) + (block_size / sizeof (int) );

        #ifdef SHOW_DEBUG
        cout << "bottom_signature:" << bottom_signature <<  " top_signature:" << top_signature << endl;
        #endif

        if (bottom_signature < BEGINNING_POINTER)
        {
            throw std::invalid_argument ("Pointer to deallocate is outside (below) memory!");
        }
        else if (bottom_signature > ENDING_POINTER)
        {
            throw std::invalid_argument ("Pointer to deallocate is outside (above) memory!");
        }
        else if (*bottom_signature != -block_size)
        {
            #ifdef SHOW_DEBUG
            cout << "bottom_signature:" << bottom_signature << " sig:" << *bottom_signature << " Expected:" << -block_size << endl;
            #endif
            throw std::invalid_argument ("Pointer is not start of block!");
        }
        else if (*top_signature != -block_size)
        {
            #ifdef SHOW_DEBUG
            cout << "top_signature:" << top_signature << " sig:" << *top_signature << " Expected:" << -block_size << endl;
            #endif
            throw std::invalid_argument ("End of block is not actually the end of a block!");
        }

        *bottom_signature = block_size;
        *top_signature    = block_size;

        int coalesced_block_size = block_size;

        // If previous block exists and is free, coalesce it with this
        if (bottom_signature != BEGINNING_POINTER)
        {
            int* previous_block_top_signature = bottom_signature - 1;

            if (*previous_block_top_signature >= 0)
            {
                int previous_block_size = *previous_block_top_signature;
                int* previous_block_bottom_signature = previous_block_top_signature - (previous_block_size / sizeof (int) ) - 1;
                bottom_signature = previous_block_bottom_signature;

                #ifdef SHOW_DEBUG
                cout << "Merging buffer with PREVIOUS buffer of size " << previous_block_size << endl;
                #endif

                coalesced_block_size += previous_block_size + (2 * sizeof (int) );
                *bottom_signature = coalesced_block_size;
                *top_signature = coalesced_block_size;
            }
        }

        // If next block exists and is free, coalesce it with this
        if (top_signature != ENDING_POINTER - 1)
        {
            int* next_block_bottom_signature = top_signature + 1;

            if (*next_block_bottom_signature >= 0)
            {
                int  next_block_size          = *next_block_bottom_signature;
                int* next_block_top_signature = next_block_bottom_signature + (next_block_size / sizeof (int) ) + 1;

                top_signature = next_block_top_signature;

                #ifdef SHOW_DEBUG
                cout << "Merging buffer with NEXT buffer of size " << next_block_size << endl;
                #endif

                coalesced_block_size += next_block_size + (2 * sizeof (int) );
                *bottom_signature = coalesced_block_size;
                *top_signature    = coalesced_block_size;
            }
        }

        assert (valid() );
    }


    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p)
    {
        p->~T();               // this is correct
        assert (valid() );
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i)
    {
        return *reinterpret_cast<int*> (&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const
    {
        return *reinterpret_cast<const int*> (&a[i]);
    }

};

#endif // Allocator_h
