#include <vector>
#include <iostream>
#include <list>
#include <sstream>

using namespace std;

#include "stdafx.h"
#include "Allocator.h"

#define VALUE_TYPE long
#define POINTER_TYPE long*
#define ALLOCATION_SIZE 1000

list< vector<int> > process_input (istream& input, ostream& output, int& num_cases)
{
    list< vector<int> > cases = {};

    // Get num cases
    input >> num_cases;
    string tmp;
    getline (input, tmp);

    // Skip blank line
    getline (input, tmp);

    // process cases
    for (int i = 0; i < num_cases; i++)
    {
        vector<int> current_case = {};

        getline (input, tmp);

        while ( (tmp.compare ("") != 0) && (tmp.compare ("\r") != 0) )
        {
            stringstream ss (tmp);

            int request;
            ss >> request;
            current_case.push_back (request);
            getline (input, tmp);
        }

        cases.push_back (current_case);
    }

    return cases;
}


inline void do_allocate_request (ostream& output,
                                 my_allocator<VALUE_TYPE, ALLOCATION_SIZE>& alloc,
                                 int objects_to_allocate)
{
    #ifdef SHOW_DEBUG
    output << "Allocating " << objects_to_allocate << " objects..." << endl;
    #endif

    size_t objects = objects_to_allocate;
    POINTER_TYPE location = alloc.allocate (objects);

    if (location != 0)
    {
        #ifdef SHOW_DEBUG
        output << "Allocated to location " << location << endl;
        #endif
    }
}

inline void do_deallocate_request (ostream& output,
                                   my_allocator<VALUE_TYPE, ALLOCATION_SIZE>& alloc,
                                   int block_to_deallocate)
{
    #ifdef SHOW_DEBUG
    cout << "Deallocating block " << block_to_deallocate << endl;
    #endif

    int count = 0;
    int alloc_index = 0;

    while (alloc_index < ALLOCATION_SIZE)
    {
        int sig_val = alloc[alloc_index];

        //int block_size_in_ints = abs(sig_val) / sizeof(int);
        if (sig_val < 0)
        {
            ++count;

            if (count == abs (block_to_deallocate) )
            {
                alloc.deallocate (reinterpret_cast<POINTER_TYPE> (& (alloc[alloc_index + sizeof (int)]) ),
                                  abs (sig_val) / sizeof (VALUE_TYPE) );
                break;
            }
        }

        alloc_index += abs (sig_val) + (2 * sizeof (int) );
    }

    assert (alloc_index < ALLOCATION_SIZE);
}


inline void print_results (ostream& output,
                           my_allocator<VALUE_TYPE, ALLOCATION_SIZE>& alloc)
{
    int alloc_index = 0;

    while (alloc_index < ALLOCATION_SIZE)
    {
        if (alloc_index > 0)  output << ' ';

        int sig_val = alloc[alloc_index];

        output << sig_val;

        //int block_size_in_ints = abs(sig_val) / sizeof(int);
        alloc_index += abs (sig_val) + (2 * sizeof (int) );
    }

    output << endl;
}

int do_allocate_case (ostream& output, vector<int>& current_case)
{
    my_allocator<VALUE_TYPE, ALLOCATION_SIZE> alloc;

    for (vector<int>::iterator current_request = current_case.begin();
            current_request != current_case.end();
            current_request++)
    {
        //cout << "Request: " << *current_request << endl;
        if (*current_request > 0)
        {
            do_allocate_request (output, alloc, *current_request);
            //print_results(output, alloc);
        }
        else
        {
            do_deallocate_request (output, alloc, *current_request);
        }
    }

    print_results (output, alloc);

    return 0;
}


int do_allocate (ostream& output, list< vector<int> >& cases)
{

    // Iterate through the cases
    for (
        list< vector<int> >::iterator current_case = cases.begin();
        current_case != cases.end();
        current_case++
    )
    {
        do_allocate_case (output, *current_case);
    }

    return 0;
}

