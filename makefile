.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

FILES :=              \
    .gitignore        \
    stdafx.h          \
    Allocator.h       \
    Allocator.c++     \
    makefile          \
    RunAllocator.c++  \
    RunAllocator.h    \
    RunAllocator.in   \
    RunAllocator.out  \
    jacobosborne-RunAllocator.in  \
    jacobosborne-RunAllocator.out \
    TestAllocator.c++ \

allocator-tests:
	git clone https://gitlab.com/gpdowning/cs371p-allocator-tests.git allocator-tests

html: Doxyfile Allocator.h
	doxygen Doxyfile

Allocator.log:
	git log > Allocator.log

Doxyfile:
	doxygen -g

RunAllocator: stdafx.h Allocator.h Allocator.c++ RunAllocator.h RunAllocator.c++
	-cppcheck Allocator.h
	-cppcheck RunAllocator.c++
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra Allocator.h RunAllocator.c++ -o RunAllocator

RunAllocator.c++x: RunAllocator
	./RunAllocator < jacobosborne-RunAllocator.in > jacobosborne-RunAllocator.tmp
	-diff jacobosborne-RunAllocator.tmp jacobosborne-RunAllocator.out

TestAllocator: stdafx.h Allocator.h Allocator.c++ RunAllocator.h RunAllocator.c++
	-cppcheck Allocator.h
	-cppcheck TestAllocator.c++
	g++ -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -Wall -Weffc++ -Wextra Allocator.c++ TestAllocator.c++ -o TestAllocator -lgtest -lgtest_main -pthread

TestAllocator.c++x: TestAllocator
	valgrind ./TestAllocator
	gcov -b TestAllocator.c++ | grep -A 5 "File '.*Allocator.h'"

all: RunAllocator

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunAllocator
	rm -f TestAllocator

config:
	git config -l

ctd:
	checktestdata TestAllocator.ctd jacobosborne-RunAllocator.in

docker:
	docker run -it -v $(PWD):/usr/allocator -w /usr/allocator gpdowning/gcc

format:
	astyle stdafx.h -A1 -xw -f -p -xg -d
	astyle Allocator.h -A1 -xw -f -p -xg -d
	astyle Allocator.c++ -A1 -xw -f -p -xg -d
	astyle RunAllocator.h -A1 -xw -f -p -xg -d
	astyle RunAllocator.c++ -A1 -xw -f -p -xg -d
	astyle TestAllocator.c++ -A1 -xw -f -p -xg -d

init:
	touch README
	git init
	git remote add origin git@gitlab.com/gpdowning/cs371p-allocator.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

run: RunAllocator.c++x TestAllocator.c++x

scrub:
	make clean
	rm -f Allocator.log
	rm -f Doxyfile
	rm -rf allocator-tests
	rm -rf html
	rm -rf latex

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

versions:
	which astyle
	astyle --version
	@echo
	dpkg -s libboost-dev | grep 'Version'
	@echo
	which checktestdata
	checktestdata --version
	@echo
	which cmake
	cmake --version
	@echo
	which cppcheck
	cppcheck --version
	@echo
	which doxygen
	doxygen --version
	@echo
	which g++
	g++ --version
	@echo
	which gcov
	gcov --version
	@echo
	which git
	git --version
	@echo
	which make
	make --version
	@echo
	which valgrind
	valgrind --version
	@echo
	which vim
	vim --version
	
