#include <vector>
#include <iostream>
#include <list>
#include <sstream>

using namespace std;

#include "stdafx.h"
#include "Allocator.h"
#include "Allocator.c++"
#include "RunAllocator.h"

int main()
{
    int num_cases;
    list< vector<int> > cases = process_input (cin, cout, num_cases);
    do_allocate (cout, cases);
    return 0;
}

