#ifndef stdafx
#define stdafx

//#define SHOW_DEBUG
//#define SHOW_VALID

#define round_up_division(numerator,denominator) (numerator / denominator) \
    + ( (numerator % denominator) == 0 ? 0 : 1)

#endif
